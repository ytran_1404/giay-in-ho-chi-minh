const common = (function () {
    const _generationBanner = function () {
        $('.banner-item').slick({
            infinite: true,
            autoplay: true,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            focusOnSelect: true,
        });
        $('.slide-big-img').slick({
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4000,
            slidesToShow: 1,
            speed: 900,
            slidesToScroll: 1,
            arrows: false,
            dots: false,
            fade: true,
            asNavFor: '.slide-small-img',
            cssEase: 'ease-in-out',
            prevArrow: '<img class="icon_pre" src="resources/images/home/arrow_leftpart.png" alt="" >',
            nextArrow: '<img class="icon_next" src="resources/images/home/arrow_rightpart.png" alt="" >',
        });
        $('.slide-small-img').slick({
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4000,
            slidesToShow: 4,
            speed: 900,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            asNavFor: '.slide-big-img',
            cssEase: 'ease-in-out',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        arrows: false,
                        slidesToShow: 3,
                    }
                },
            ]
        });
        $('.slide-item-group').slick({
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4000,
            slidesToShow: 4,
            speed: 900,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            cssEase: 'ease-in-out',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false
                    }
                },
            ]
        });
        $('.slide-news-group').slick({
            infinite: true,
            autoplay: false,
            autoplaySpeed: 4000,
            slidesToShow: 3,
            speed: 900,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            cssEase: 'ease-in-out',
            responsive: [
                {
                    breakpoint: 1024,
                    settings: {
                        arrows: false,
                        slidesToShow: 2,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                    }
                },
                {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1,
                        arrows:false
                    }
                },
            ]
        });
    };
    const _toToppage = function () {
        window.addEventListener('scroll', function (event) {
            const scroll = this.scrollY;
            const x = document.querySelector('#totop');
            if (scroll > 500) {
                (x).style.display = 'block';
            } else {
                (x).style.display = 'none';
            }
        });
        $('#totop').on('click', function () {
            $('html, body').animate({
                scrollTop: ($('.content-container').offset().top)
            }, 0);
        });
    };
    const _toContact = function () {
        $('.click').on('click', function () {
            $('html, body').animate({
                scrollTop: ($('footer').offset().top)
            }, 0);
        })
    };
    return {
        init: function () {
            _generationBanner();
            _toToppage();
            _toContact();
        }
    };
})();
common.init();